
export class Pessoa
{
    id:number = 0
    nome:string = ""
    idade:number = 0
    cpf:string = ""
    temFilhos:boolean
    dataNascimento:Date
}