import { Module } from '@nestjs/common';
import { PessoaController } from './controllers/pessoa/pessoa.controller';
import { UteisController } from './controllers/uteis/uteis.controller';
import { CursoController } from './curso/curso.controller';

@Module({
  imports: [],
  controllers: [PessoaController, UteisController, CursoController ],
  providers: [],
})
export class AppModule {}
