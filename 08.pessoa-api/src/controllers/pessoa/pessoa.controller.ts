import { Controller, Get, Query, Param } from '@nestjs/common';
import { Pessoa } from '../../models/pessoa'

@Controller('pessoa')
export class PessoaController {

    @Get('obternome')
    obterNome():string
    {
        return "Jackson Pimentel"
    }

    @Get('obternome/:ambiente')
    obterNome2(@Param('ambiente') ambiente):string
    {
        return "Jackson Pimentel em " + ambiente
    }

    @Get('obterpessoa')
    obterPessoa():Pessoa
    {
        const pessoa = new Pessoa()

        pessoa.idade = 36
        pessoa.nome = "Jackson Pimentel"

        return pessoa
    }

    @Get('obterpessoa2/:id')
    obterPessoa2(@Param('id') id:number):Pessoa
    {
        const pessoa = new Pessoa()

        pessoa.id = id
        pessoa.idade = 36
        pessoa.nome = "Jackson Pimentel"

        return pessoa
    }

    @Get('obterpessoa2/:id/:nome')
    obterPessoa3(@Param('id') id:number, @Param('nome') nome:string):Pessoa
    {
        const pessoa = new Pessoa()

        pessoa.id = id
        pessoa.idade = 36
        pessoa.nome = nome

        return pessoa
    }    
    
    @Get('buscapessoa')
    buscaPessoa(@Query('id') id:number, @Query('cpf') cpf:string):Pessoa
    {
        const pessoa = new Pessoa()

        if(id == 1)
        {
            pessoa.id = id;
            pessoa.nome = "Jackson"
            pessoa.cpf = cpf
            pessoa.temFilhos = true
            pessoa.dataNascimento = new Date('1983-06-04 18:40:00 GMT')
        }
        else if(id == 2)
        {
            pessoa.id = id;
            pessoa.nome = "Lucas"
            pessoa.cpf = cpf
            pessoa.temFilhos = false
            pessoa.dataNascimento = new Date('2003-05-08 00:00:00')
        }
        else if(id == 3)
        {
            pessoa.id = id;
            pessoa.nome = "Karine"
            pessoa.cpf = cpf
            pessoa.temFilhos = false
            pessoa.dataNascimento = new Date('2002-07-05 12:00:00')
        }

        return pessoa
    }


}
