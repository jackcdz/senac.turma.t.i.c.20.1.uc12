import { Controller, Get, Query, Param } from '@nestjs/common';
import { AreaTerreno } from 'src/models/area-terreno';
import { VendaPadaria } from 'src/models/venda-padaria';
import { Combustivel } from 'src/models/combustivel';

@Controller('uteis')
export class UteisController {

    /*
        http://localhost:3000/uteis/areaterreno?largura=50&comprimento=12
    */
    @Get('areaterreno')
    areaTerreno(
        @Query('largura') largura:number, 
        @Query('comprimento') comprimento:number):AreaTerreno
    {
        const area = largura * comprimento

        const areaTerreno = new AreaTerreno()
        
        areaTerreno.area = area

        return areaTerreno

        /*
        return  {
                    area
                }*/
    }

    /*
        http://localhost:3000/uteis/calculovendapadaria?broas=5&paes=10
    */
    @Get('calculovendapadaria')
    calculoVendaPadaria(
        @Query('paes') paes:number,
        @Query('broas') broas:number):VendaPadaria
    {
        const totalVendaPaes = paes * 0.12
        const totalVendaBroas = broas * 1.5
        const totalVendas = totalVendaPaes + totalVendaBroas
        const totalPoupanca = totalVendas * 0.10

        return  {
                    totalVendaPaes,
                    totalVendaBroas,
                    totalVendas,
                    totalPoupanca
                }
    }

    /*
        http://localhost:3000/uteis/calculoabastecimento/4/40.50
    */
    @Get('calculoabastecimento/:valorlitro/:totalabastecido')
    calculoAbastecimento(
        @Param('valorlitro') valorLitro:number,
        @Param('totalabastecido') totalAbastecido:number):Combustivel
    {
        const combustivel = new Combustivel

        combustivel.litros = totalAbastecido / valorLitro

        return combustivel
    }
}
