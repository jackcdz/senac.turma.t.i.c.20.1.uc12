import { Controller, Post, Body, Put, Param, Get, Query } from '@nestjs/common';
import { Curso } from 'src/models/curso';

@Controller('curso')
export class CursoController {

    private static cursos:Curso[] = []
    private static sequenciaId:number = 0

    /*
    POST http://localhost:3000/curso/criar

    {
        "descricao":"Tecnico em Seg Trab",
        "codigo":"SEN777533",
        "ativo":true
    }

    */
    @Post('/criar')
    criar(@Body() curso:Curso):Curso
    {
        CursoController.sequenciaId++

        curso.id = CursoController.sequenciaId

        CursoController.cursos.push(curso)

        return curso
    }

   /*
    PUT http://localhost:3000/curso/alterar/2

    {
        "descricao":"Tecnico em Seg Trab",
        "codigo":"SEN777533",
        "ativo":true
    }
    
    */
    @Put('/alterar/:id')
    alterar(@Param('id') idParam:string, @Body() curso:Curso):Curso
    {
        const id = parseInt(idParam)

        for(let i = 0; i < CursoController.cursos.length; i++)
        {
            if(CursoController.cursos[i].id == id)
            {
                curso.id = id
                CursoController.cursos[i] = curso
                break;
            }
        }

        return curso
    }

    //http://localhost:3000/curso/buscaporid?id=2
    @Get('/buscaporid')
    buscaPorId(@Query('id') id:number):Curso
    {
        for (const curso of CursoController.cursos) {
            if(curso.id == id)
                return curso
        }

        return null
    }

    //http://localhost:3000/curso/buscartodos
    @Get('/buscartodos')
    buscarTodos():Curso[]
    {
        return CursoController.cursos
    }
}
