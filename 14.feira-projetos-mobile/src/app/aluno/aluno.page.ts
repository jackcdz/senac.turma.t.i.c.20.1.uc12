import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-aluno',
  templateUrl: './aluno.page.html',
  styleUrls: ['./aluno.page.scss'],
})
export class AlunoPage implements OnInit {

  constructor(private navController:NavController) 
  { 

  }

  ngOnInit() {
  }

  async navAlunoInserir()
  {
    this.navController.navigateForward('aluno-edicao/')
  }
}