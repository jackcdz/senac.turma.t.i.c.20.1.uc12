import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private navController:NavController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  async navHome()
  {
    this.navController.navigateForward('home')
  }

  async navCurso()
  {
    this.navController.navigateForward('curso')
  }
 
  async navAluno()
  {
    this.navController.navigateForward('aluno')
  }

  async navEquipe()
  {
    this.navController.navigateForward('equipe')
  }

  async navProjeto()
  {
    this.navController.navigateForward('projeto')
  }

  

}
