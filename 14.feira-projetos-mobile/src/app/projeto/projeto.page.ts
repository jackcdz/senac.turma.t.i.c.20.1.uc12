import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-projeto',
  templateUrl: './projeto.page.html',
  styleUrls: ['./projeto.page.scss'],
})
export class ProjetoPage implements OnInit {

  valor:number

  conversao:string

  resultado:number

  cotacao:number

  constructor() { }

  ngOnInit() {
  }

  async converter()
  {
    if(this.conversao == "REAL")
      this.resultado = this.valor / this.cotacao
    
    if(this.conversao == "DOLAR")
      this.resultado = this.valor * this.cotacao
  }

}
