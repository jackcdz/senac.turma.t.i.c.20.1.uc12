
export interface Curso
{
    id:string
    nome:string
    coordenador:string
}

export interface Equipe
{
    id:string
    nome:string
    gritoguerra:string
}