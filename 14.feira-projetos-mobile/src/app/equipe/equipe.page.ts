import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { Equipe } from '../models';
import { EquipeService } from '../services/equipe.service';
import { map, catchError } from 'rxjs/operators'

@Component({
  selector: 'app-equipe',
  templateUrl: './equipe.page.html',
  styleUrls: ['./equipe.page.scss'],
})
export class EquipePage implements OnInit {

 
  equipes:Equipe[] = []

  constructor(
    private navController:NavController,
    private alertController:AlertController,
    private equipeService:EquipeService
    ) 
  { 

  }

  ngOnInit() 
  {
    const obs = this.equipeService.obterTodos()
              .valueChanges()
              .pipe(
                map(equipes => this.equipes = equipes)
              )

    obs.subscribe()
  }

  async navEquipeInserir()
  {
    this.navController.navigateForward('equipe-edicao/')
  }

  async navEquipeAlterar(equipe:Equipe)
  {
    this.navController.navigateForward(`equipe-edicao/${equipe.id}`)
  }

  async excluirEquipe(equipe:Equipe)
  {
    const alert = await this.alertController.create(
      {
        header:"Feira de Projetos",
        message:`Confirma exclusão do equipe ${equipe.nome}?`,
        buttons:[
          {
            text:"Cancelar",
            cssClass:"secondary"
          },
          {
            text:"Ok",
            handler:() => this.excluirEquipeAcao(equipe)
          }
        ]
      }
    )

    await alert.present()
  }

  excluirEquipeAcao(equipe:Equipe)
  {
    this.equipeService.excluir(equipe.id)
    this.ngOnInit()
  }
}
