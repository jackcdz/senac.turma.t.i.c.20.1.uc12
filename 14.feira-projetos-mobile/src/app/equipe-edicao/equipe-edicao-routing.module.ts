import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EquipeEdicaoPage } from './equipe-edicao.page';

const routes: Routes = [
  {
    path: '',
    component: EquipeEdicaoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EquipeEdicaoPageRoutingModule {}
