import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Equipe } from '../models';
import { EquipeService } from '../services/equipe.service';
import { map, catchError } from 'rxjs/operators'

@Component({
  selector: 'app-equipe-edicao',
  templateUrl: './equipe-edicao.page.html',
  styleUrls: ['./equipe-edicao.page.scss'],
})
export class EquipeEdicaoPage implements OnInit {

  
  equipe: Equipe

  constructor(
    private navController: NavController,
    private toastContrller: ToastController,
    private activateRoute: ActivatedRoute,
    private equipeService:EquipeService
  ) {

    let id:string = this.activateRoute.snapshot.paramMap.get('id')

    this.equipe = 
    {
      id:"",
      nome:"",
      gritoguerra:""
    }

    if(id != '')
    {
      const obs = this.equipeService.obter(id)
                  .valueChanges()
                  .pipe(map(equipe => this.equipe = equipe))
      obs.subscribe()
    }
  }

  ngOnInit() {
  }

  async mensagemSalvoSucesso() {
    const toast = await this.toastContrller.create(
      {
        header: 'Feira de Projetos',
        message: 'Equipe salvo com sucesso',
        duration: 3000
      }
    )

    toast.present()
  }

  async salvar() {

    if(this.equipe.id == "")
      this.equipeService.inserir(this.equipe)
    else
      this.equipeService.alterar(this.equipe)

    this.navController.navigateBack('equipe')
    this.mensagemSalvoSucesso()
  }

  
}
