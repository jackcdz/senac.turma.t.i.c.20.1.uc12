import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EquipeEdicaoPageRoutingModule } from './equipe-edicao-routing.module';

import { EquipeEdicaoPage } from './equipe-edicao.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EquipeEdicaoPageRoutingModule
  ],
  declarations: [EquipeEdicaoPage]
})
export class EquipeEdicaoPageModule {}
