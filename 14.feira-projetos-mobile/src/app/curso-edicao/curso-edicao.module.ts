import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CursoEdicaoPageRoutingModule } from './curso-edicao-routing.module';

import { CursoEdicaoPage } from './curso-edicao.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CursoEdicaoPageRoutingModule
  ],
  declarations: [CursoEdicaoPage]
})
export class CursoEdicaoPageModule {}
