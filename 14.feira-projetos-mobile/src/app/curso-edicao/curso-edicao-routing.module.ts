import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CursoEdicaoPage } from './curso-edicao.page';

const routes: Routes = [
  {
    path: '',
    component: CursoEdicaoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CursoEdicaoPageRoutingModule {}
