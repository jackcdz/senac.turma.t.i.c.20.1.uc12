import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, NavParams, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Curso } from '../models';
import { CursoService } from '../services/curso.service';
import { map, catchError } from 'rxjs/operators'

@Component({
  selector: 'app-curso-edicao',
  templateUrl: './curso-edicao.page.html',
  styleUrls: ['./curso-edicao.page.scss'],
})
export class CursoEdicaoPage implements OnInit {

  curso: Curso

  constructor(
    private navController: NavController,
    private toastContrller: ToastController,
    private activateRoute: ActivatedRoute,
    private cursoService:CursoService,
    private alertController:AlertController
  ) {

    let id:string = this.activateRoute.snapshot.paramMap.get('id')

    this.curso = 
    {
      id:"",
      nome:"",
      coordenador:""
    }

    if(id != '')
    {
      const obs = this.cursoService.obter(id)
                  .valueChanges()
                  .pipe(map(curso => this.curso = curso))
      obs.subscribe()
    }
  }

  ngOnInit() {
  }

  async mensagemSalvoSucesso() {
    const toast = await this.toastContrller.create(
      {
        header: 'Feira de Projetos',
        message: 'Curso salvo com sucesso',
        duration: 3000
      }
    )

    toast.present()
  }

  async mostrarTermos()
  {
    const alert = await this.alertController.create({
      header: 'Termos',
      subHeader: 'Termos de Privacidade',
      message: 'Nunca é demais lembrar o peso e o significado destes problemas, uma vez que a estrutura atual da organização promove a alavancagem dos conhecimentos estratégicos para atingir a excelência.',
      buttons: [
        {
          text: 'Não Concordo',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            
          }
        }, {
          text: 'Concordo',
          handler: () => {
            this.confirmarCadastro()
          }
        }
      ]
    });

    await alert.present();
  }

  async confirmarCadastro()
  {
    if(this.curso.id == "")
      this.cursoService.inserir(this.curso)
    else
      this.cursoService.alterar(this.curso)

    this.navController.navigateBack('curso')
    this.mensagemSalvoSucesso()
  }

  async salvar() {
    this.mostrarTermos()
 
  }

}
