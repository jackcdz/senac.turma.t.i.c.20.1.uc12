import { Component, OnInit } from '@angular/core';
import {  AlertController, NavController } from '@ionic/angular';
import { Curso } from '../models';
import { CursoService } from '../services/curso.service';
import { map, catchError } from 'rxjs/operators'


@Component({
  selector: 'app-curso',
  templateUrl: './curso.page.html',
  styleUrls: ['./curso.page.scss'],
})
export class CursoPage implements OnInit {

  cursos:Curso[] = []

  constructor(
    private navController:NavController,
    private alertController:AlertController,
    private cursoService:CursoService
    ) 
  { 

  }

  ngOnInit() 
  {
    const obs = this.cursoService.obterTodos()
              .valueChanges()
              .pipe(
                map(cursos => this.cursos = cursos)
              )

    obs.subscribe()
  }

  async navCursoInserir()
  {
    this.navController.navigateForward('curso-edicao/')
  }

  async navCursoAlterar(curso:Curso)
  {
    this.navController.navigateForward(`curso-edicao/${curso.id}`)
  }

  async excluirCurso(curso:Curso)
  {
    const alert = await this.alertController.create(
      {
        header:"Feira de Projetos",
        message:`Confirma exclusão do curso ${curso.nome}?`,
        buttons:[
          {
            text:"Cancelar",
            cssClass:"secondary"
          },
          {
            text:"Ok",
            handler:() => this.excluirCursoAcao(curso)
          }
        ]
      }
    )

    await alert.present()
  }

  excluirCursoAcao(curso:Curso)
  {
    this.cursoService.excluir(curso.id)
    this.ngOnInit()
  }


}
