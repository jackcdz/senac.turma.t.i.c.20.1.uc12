import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'curso',
    loadChildren: () => import('./curso/curso.module').then( m => m.CursoPageModule)
  },
  {
    path: 'aluno',
    loadChildren: () => import('./aluno/aluno.module').then( m => m.AlunoPageModule)
  },
  {
    path: 'equipe',
    loadChildren: () => import('./equipe/equipe.module').then( m => m.EquipePageModule)
  },
  {
    path: 'equipe-edicao/:id',
    loadChildren: () => import('./equipe-edicao/equipe-edicao.module').then( m => m.EquipeEdicaoPageModule)
  },
  {
    path: 'projeto',
    loadChildren: () => import('./projeto/projeto.module').then( m => m.ProjetoPageModule)
  },
  {
    path: 'curso-edicao/:id',
    loadChildren: () => import('./curso-edicao/curso-edicao.module').then( m => m.CursoEdicaoPageModule)
  },
  {
    path: 'aluno-edicao/:id',
    loadChildren: () => import('./aluno-edicao/aluno-edicao.module').then( m => m.AlunoEdicaoPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
