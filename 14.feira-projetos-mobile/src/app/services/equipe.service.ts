import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore'
import { Equipe } from '../models';

@Injectable({
  providedIn: 'root'
})
export class EquipeService {

  constructor(private firestore:AngularFirestore) { }

  inserir(equipe:Equipe)
  {
    let id:string = this.firestore.createId();

    equipe.id = id

    return this.firestore.doc(`equipes/${id}`).set(equipe)
  }

  alterar(equipe:Equipe)
  {
    return this.firestore.doc(`equipes/${equipe.id}`).set(equipe)
  }

  obterTodos():AngularFirestoreCollection<Equipe>
  {
    return this.firestore.collection(`equipes`)
  }

  obter(id:string):AngularFirestoreDocument<Equipe>
  {
    return this.firestore.collection(`equipes`).doc(id)
  }

  excluir(id:string)
  {
    return this.firestore.doc(`equipes/${id}`).delete()
  }
}
