import { Injectable } from '@angular/core';
import { Curso } from '../models';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore'

@Injectable({
  providedIn: 'root'
})
export class CursoService {

  constructor(private firestore:AngularFirestore) { }

  inserir(curso:Curso)
  {
    let id:string = this.firestore.createId();

    curso.id = id

    return this.firestore.doc(`cursos/${id}`).set(curso)
  }

  alterar(curso:Curso)
  {
    return this.firestore.doc(`cursos/${curso.id}`).set(curso)
  }

  obterTodos():AngularFirestoreCollection<Curso>
  {
    return this.firestore.collection(`cursos`)
  }

  obter(id:string):AngularFirestoreDocument<Curso>
  {
    return this.firestore.collection(`cursos`).doc(id)
  }

  excluir(id:string)
  {
    return this.firestore.doc(`cursos/${id}`).delete()
  }

}
