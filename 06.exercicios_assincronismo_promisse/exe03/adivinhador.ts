
export class Adivinhador
{
    async verificar(numero:number):Promise<number>
    {
        return new Promise<number>((success, failure) =>
        {
            if(numero >= 0)
            {
                let random:number = -1

                while(random != numero)
                {
                    random = Math.floor(Math.random() * 10001)
                    console.log(random)
                }

                success(random)
            }
            else
                failure(new Error('Nenhuma numero informado'))
        })
    }

}