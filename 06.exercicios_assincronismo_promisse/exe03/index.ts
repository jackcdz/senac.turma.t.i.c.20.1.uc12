
import { prompt } from 'readline-sync'
import { Adivinhador } from './adivinhador'

console.log('Digite numero: ')
const n1 = parseInt(prompt())

const adivinhador = new Adivinhador()

adivinhador.verificar(n1)
        .then(result => console.log(`O número escolhido foi ${result}`))
        .catch(err => console.log(err));

console.log('Aguardando a adivinhação do número')