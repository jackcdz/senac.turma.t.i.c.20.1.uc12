
export class Funcoes
{
    async obterNumeroLetras(frase:string):Promise<number>
    {
        return new Promise<number>((success, failure) =>
        {
            setTimeout(() => 
            {
                if(frase != "")
                    success(frase.length)
                else
                    failure(new Error('Nenhuma frase informada'))
            }, 5000)
        })
    }

}