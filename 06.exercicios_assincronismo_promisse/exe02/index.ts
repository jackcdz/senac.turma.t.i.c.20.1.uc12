
import { prompt } from 'readline-sync'
import { Funcoes } from './funcoes'

console.log('Digite uma frase: ')
const frase = prompt()

const funcoes = new Funcoes()

funcoes.obterNumeroLetras(frase)
        .then(result => console.log(`A frase possuí ${result} letras`))
        .catch(err => console.log(err));

console.log('Aguardando contagem de letras da frase')