
import { prompt } from 'readline-sync'
import { Calculador } from './calculador'

console.log('Digite o primeiro numero: ')
const n1 = parseInt(prompt())

console.log('Digite o segundo numero: ')
const n2 = parseInt(prompt())

console.log('Digite o terceiro numero: ')
const n3 = parseInt(prompt())

console.log('O que deseja calcular (A/P)?')
const op = prompt().toUpperCase()

const calculator = new Calculador()

if(op == 'A')
{
    calculator.calcularMediaAritmetica(n1, n2, n3)
        .then(result => console.log(`A média aritmética é ${result}`))
        .catch(err => console.log(err));
}
else if(op == 'P')
{
    calculator.calcularMediaPondera(n1, n2, n3)
        .then(result => console.log(`A média ponderada é ${result}`))
        .catch(err => console.log(err));
}

console.log('Aguardando calculo da média')