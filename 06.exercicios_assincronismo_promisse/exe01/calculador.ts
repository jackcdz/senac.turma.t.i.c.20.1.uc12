
export class Calculador
{
    async calcularMediaAritmetica(n1:number, n2:number, n3:number):Promise<number>
    {
        return new Promise<number>((success, failure) =>
        {
            setTimeout(() => 
            {
                const soma = n1 + n2 + n3

                if(soma > 0)
                {
                    const media = soma / 3
                    success(media)
                }
                else
                    failure(new Error('Nenhum valor informado'))
            }, 5000)
        })
    }

    async calcularMediaPondera(n1:number, n2:number, n3:number):Promise<number>
    {
        return new Promise<number>((success, failure) =>
        {
            setTimeout(() => 
            {
                if(n1 > 0 && n2 > 0 && n3 > 0)
                {
                    const media = (n1*1+n2*2+n3*3)/(1+2+3)
                    success(media)
                }
                else
                    failure(new Error('Nenhum valor informado'))
            }, 5000)
        })
    }
}