import { Injectable } from '@nestjs/common';
import { Produto } from 'src/models/produto';
import { ProdutoProvider } from 'src/providers/produto/produto-provider';
import { exception } from 'console';

@Injectable()
export class ProdutoService {

    constructor(private produtoProvider:ProdutoProvider)
    {
    }

    async salvar(produto:Produto):Promise<Produto>
    {
        produto.descricao = produto.descricao.toUpperCase();

        if(produto.preco < 0)
        {
            throw new exception('Preco não pode ser menor que 0')
        }

        if(produto.id == 0)
        {
            const produtoSalvo = await this.produtoProvider.inserir(produto)
            return produtoSalvo
        }
        else
        {
            const produtoSalvo = await this.produtoProvider.alterar(produto)
            return produtoSalvo
        }
    }
}
