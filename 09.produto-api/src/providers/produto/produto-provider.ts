import { Injectable } from '@nestjs/common';
import { Produto } from 'src/models/produto';

@Injectable()
export class ProdutoProvider {
    
    private static produtos:Produto[] = []

    private static sequenciaId:number = 0

    async inserir(produto:Produto):Promise<Produto>
    {        
        return new Promise((successs, failed) =>
            {
                ProdutoProvider.sequenciaId++

                produto.id = ProdutoProvider.sequenciaId
                
                ProdutoProvider.produtos.push(produto)

                successs(produto)
            }
        )
    }

    async alterar(produto:Produto):Promise<Produto>
    {
        return new Promise((successs, failed) =>
        {
            for (let index = 0; index < ProdutoProvider.produtos.length; index++) {
                
                const produtoItem = ProdutoProvider.produtos[index];
                
                if(produtoItem.id == produto.id)
                {
                    ProdutoProvider.produtos[index] = produto
                    break
                }
            }

            successs(produto)
        })
    }

    async obterTodos():Promise<Produto[]>
    {
        return new Promise((success, failed) =>
            {
                success(ProdutoProvider.produtos)
            }
        )
    }

    async obterPorId(id:number):Promise<Produto>
    {
        return new Promise((success, failed) =>
            {
                for (const produto of ProdutoProvider.produtos) {
                    if(produto.id == id)
                    {
                        success(produto)
                        return
                    }
                }

                success(null)
            }
        )
    }

}
