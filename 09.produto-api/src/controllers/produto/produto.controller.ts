import { Controller, Post, Body, Param, Put, Get, Query } from '@nestjs/common';
import { Produto } from 'src/models/produto';
import { ProdutoService } from 'src/services/produto/produto.service';
import { ProdutoProvider } from 'src/providers/produto/produto-provider';

@Controller('produto')
export class ProdutoController {

    constructor(
        private produtoService:ProdutoService,
        private produtoProvider:ProdutoProvider
        )
    {

    }

    @Post('/criar')
    async criar(@Body() produto:Produto):Promise<Produto>
    {
        produto.id = 0
        produto = await this.produtoService.salvar(produto)
        return produto
    }

    @Put('/alterar/:id')
    async alterar(@Param('id') idParam:string, @Body() produto:Produto):Promise<Produto>
    {
        produto.id = parseInt(idParam)
        produto = await this.produtoService.salvar(produto)        
        return produto
    }

    @Get('/obterporid')
    async obterPorId(@Query('id') id:number):Promise<Produto>
    {
        return this.produtoProvider.obterPorId(id)
    }

    @Get('/obtertodos')
    async obterTodos():Promise<Produto[]>
    {
        return this.produtoProvider.obterTodos()
    }


}
