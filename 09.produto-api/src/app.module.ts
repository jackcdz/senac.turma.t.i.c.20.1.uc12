import { Module } from '@nestjs/common';

import { ProdutoController } from './controllers/produto/produto.controller';
import { ProdutoService } from './services/produto/produto.service';
import { ProdutoProvider } from './providers/produto/produto-provider';

import { CategoriaController } from './controllers/categoria/categoria.controller';
import { CategoriaService } from './services/categoria/categoria.service';
import { CategoriaProvider } from './providers/categoria/categoria-provider';

@Module({
  imports: [],
  controllers: [ProdutoController,CategoriaController],
  providers: [ProdutoService, ProdutoProvider, CategoriaProvider, CategoriaService],
})
export class AppModule {}
