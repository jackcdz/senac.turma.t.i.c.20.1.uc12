
export class Produto
{
    id:number = 0
    descricao:string = ""
    preco:number = 0
    dataCadastro:Date = new Date()
    ativo:boolean = false
}