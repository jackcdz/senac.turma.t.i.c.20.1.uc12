import { PlanoSaudeProvider } from "./providers/plano-saude-provider";

async function obterTodosPlanoSaude()
{
    const planoSaudeProvider = new PlanoSaudeProvider()

    const planos = await planoSaudeProvider.getAll()

    for (const planoSaude of planos) {
        console.log(planoSaude)
    }
}


obterTodosPlanoSaude()