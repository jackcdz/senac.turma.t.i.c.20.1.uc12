import { Departamento } from "./models/departamento";
import { DepartamentoProvider } from "./providers/departamento-provider";


async function inserirDepartamento():Promise<void>
{
    const departamentoProvider = new DepartamentoProvider()

    const dep:Departamento = new Departamento()

    dep.codigo = "123"
    dep.gerente = "Jackson"
    
    await departamentoProvider.insert(dep)
}


async function updateDepartamento() {
    const departamentoProvider = new DepartamentoProvider()

    const departamento = await departamentoProvider.getBydId(14)

    if(departamento != null)
    {
        departamento.departamentoSubordinado = await departamentoProvider.getBydId(5)
        
        await departamentoProvider.update(departamento)
    }
}

async function getDepartamento()
{
    const departamentoProvider = new DepartamentoProvider()

    const departamento = await departamentoProvider.getBydId(14)

    console.log(departamento)
}

//inserirDepartamento()
//updateDepartamento()
//getDepartamento()
