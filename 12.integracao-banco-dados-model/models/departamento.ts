
export class Departamento {
    id:number = 0
    codigo:string = ""
    gerente:string = ""
    departamentoSubordinado:Departamento | null = null
}
