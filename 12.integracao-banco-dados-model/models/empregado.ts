import { Departamento } from "./departamento"
import { PlanoSaude } from "./plano-saude"

export class Empregado
{
    matricula:string = ""
    nome:string = ""
    sobrenome:string  = ""
    departamento:Departamento | null = null
    telefone:string  = ""
    dataadmissao:Date = new Date()
    cargo:string = ""
    escolaridade:string = ""
    sexo:string  = ""
    datanascimento:Date = new Date()
    salario:number = 0
    bonus:number = 0
    comissao:number = 0
    planoSaude:PlanoSaude | null = null
    id:number = 0
}