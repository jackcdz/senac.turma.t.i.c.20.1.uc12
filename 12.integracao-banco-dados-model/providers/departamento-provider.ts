import { DataBase } from "./database"
import { ResultSetHeader, RowDataPacket } from 'mysql2/typings/mysql'
import { Departamento } from "../models/departamento"


export class DepartamentoProvider {
    private dataBase: DataBase

    constructor() {
        this.dataBase = new DataBase()
    }

    async insert(departamento: Departamento): Promise<void> {
        const sql = `
                        insert into departamento
                        (
                            codigo,
                            gerente,
                            departamentosubordinadoid
                        )
                        values
                        (
                            ?, ?, ?
                        )`

        const valores: any[] =
            [
                departamento.codigo,
                departamento.gerente,
                (departamento.departamentoSubordinado != null
                    ? departamento.departamentoSubordinado.id
                    : null)
            ]

        const conexao = this.dataBase.getConexao()

        const retorno = await conexao.execute(sql, valores)

        const result = retorno[0] as ResultSetHeader

        departamento.id = result.insertId

        conexao.end()
    }

    async update(departamento: Departamento): Promise<void> {
        const sql = `
        update departamento
        set
            codigo = ?,
            gerente = ?,
            departamentosubordinadoid = ?
        where
            id = ?`

        const valores: any[] =
            [
                departamento.codigo,
                departamento.gerente,
                (departamento.departamentoSubordinado != null
                    ? departamento.departamentoSubordinado.id
                    : null),
                departamento.id
            ]

        const conexao = this.dataBase.getConexao()

        await conexao.execute(sql, valores)

        conexao.end()
    }

    async getBydId(id: number): Promise<Departamento | null> {
        const sql = `
                select
                    id,
                    codigo,
                    gerente,
                    departamentosubordinadoid
                from
                    departamento
                where
                    id = ?
                `
        const valores: any[] = [id]

        const conexao = this.dataBase.getConexao()

        const retorno = await conexao.query(sql, valores)

        conexao.end()

        var rows = retorno[0] as RowDataPacket[]

        if (rows.length > 0) {
            const row = rows[0]

            const departamento = row as Departamento

            departamento.departamentoSubordinado = await this.getBydId(row.departamentosubordinadoid)

            return departamento
        }
        else
            return null
    }

    async getAll(): Promise<Departamento[]> {
        const sql = `
        select
            id,
            codigo,
            gerente,
            departamentosubordinadoid
        from
            departamento

        order by
            codigo
        `

        const valores: any[] = []

        const conexao = this.dataBase.getConexao()

        const retorno = await conexao.query(sql, valores)

        conexao.end()

        var rows = retorno[0] as RowDataPacket[]

        return rows as Departamento[]
    }

}