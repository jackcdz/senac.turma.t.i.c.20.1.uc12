import { DataBase } from "./database"
import { ResultSetHeader, RowDataPacket } from 'mysql2/typings/mysql'
import { PlanoSaude } from "../models/plano-saude"

export class PlanoSaudeProvider {

    private dataBase: DataBase

    constructor() {
        this.dataBase = new DataBase()
    }

    async insert(planoSaude: PlanoSaude): Promise<void> {
        const sql = `
                    insert into planosaude
                    (
                        descricao
                    )
                    values
                    (
                        ?
                    )`

        const valores: any[] =
            [
                planoSaude.descricao
            ]

        const conexao = this.dataBase.getConexao()

        const retorno = await conexao.execute(sql, valores)

        const result = retorno[0] as ResultSetHeader

        planoSaude.id = result.insertId

        conexao.end()
    }

    async getById(id: number): Promise<PlanoSaude | null> {

        const sql = `
                select
                    id,
                    descricao
                from
                    planosaude
                where
                    id = ?
                `

        const valores: any[] = [id]

        const conexao = this.dataBase.getConexao()

        const retorno = await conexao.query(sql, valores)

        conexao.end()

        var rows = retorno[0] as RowDataPacket[]

        if (rows.length > 0)
            return rows[0] as PlanoSaude
        else
            return null
    }

    async getByDescricao(descricao:string): Promise<PlanoSaude | null> {

        const sql = `
                select
                    id,
                    descricao
                from
                    planosaude
                where
                    descricao = ?
                `

        const valores: any[] = [descricao]

        const conexao = this.dataBase.getConexao()

        const retorno = await conexao.query(sql, valores)

        conexao.end()

        var rows = retorno[0] as RowDataPacket[]

        if (rows.length > 0)
            return rows[0] as PlanoSaude
        else
            return null
    }

    async getAll(): Promise<PlanoSaude[]> {
        const sql = `
                        select
                            id,
                            descricao
                        from
                            planosaude

                        order by
                            descricao
                        `

        const valores: any[] = []

        const conexao = this.dataBase.getConexao()

        const retorno = await conexao.query(sql, valores)

        conexao.end()

        var rows = retorno[0] as RowDataPacket[]

        return rows as PlanoSaude[]
    }

    async update(planosaude: PlanoSaude): Promise<void> {
        const sql = `
        update planosaude
        set
            descricao = ?
        where
            id = ?`

        const valores: any[] =
            [
                planosaude.descricao,
                planosaude.id
            ]

        const conexao = this.dataBase.getConexao()

        await conexao.execute(sql, valores)

        conexao.end()
    }

}