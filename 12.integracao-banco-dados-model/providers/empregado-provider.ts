import { Empregado } from "../models/empregado"
import { DataBase } from "./database"
import { ResultSetHeader, RowDataPacket } from 'mysql2/typings/mysql'
import { PlanoSaudeProvider } from "./plano-saude-provider"
import { DepartamentoProvider } from "./departamento-provider"

export class EmpregadoProvider {

    private dataBase: DataBase

    private planoSaudeProvider:PlanoSaudeProvider

    private departamentoProvider:DepartamentoProvider

    constructor() {
        this.dataBase = new DataBase()
        this.planoSaudeProvider = new PlanoSaudeProvider()
        this.departamentoProvider = new DepartamentoProvider()
    }

    async insert(empregado:Empregado): Promise<void> {
        const sql = `
                    insert into empregado
                    (
                        matricula,
                        nome,
                        sobrenome,
                        departamentoid,
                        telefone,
                        dataadmissao,
                        cargo,
                        escolaridade,
                        sexo,
                        datanascimento,
                        salario,
                        bonus,
                        comissao,
                        planosaudeid
                    )
                    values
                    (
                        ?,?,?,?,?,?,?,?,?,?,?,?,?,?
                    )`

        const valores: any[] =
            [
                empregado.matricula,
                empregado.nome,
                empregado.sobrenome,
                (empregado.departamento != null 
                    ? empregado.departamento.id
                    : null),
                empregado.telefone,
                empregado.dataadmissao,
                empregado.cargo,
                empregado.escolaridade,
                empregado.sexo,
                empregado.datanascimento,
                empregado.salario,
                empregado.bonus,
                empregado.comissao,
                (empregado.planoSaude != null ? empregado.planoSaude.id : null)
            ]

        const conexao = this.dataBase.getConexao()

        const retorno = await conexao.execute(sql, valores)

        const result = retorno[0] as ResultSetHeader

        empregado.id = result.insertId

        conexao.end()
    }

    async getById(id: number): Promise<Empregado | null> {

        const sql = `
                select
                    id,
                    matricula,
                    nome,
                    sobrenome,
                    departamentoid,
                    telefone,
                    dataadmissao,
                    cargo,
                    escolaridade,
                    sexo,
                    datanascimento,
                    salario,
                    bonus,
                    comissao,
                    planosaudeid
                from
                    empregado
                where
                    id = ?
                `

        const valores: any[] = [id]

        const conexao = this.dataBase.getConexao()

        const retorno = await conexao.query(sql, valores)

        conexao.end()

        var rows = retorno[0] as RowDataPacket[]

        if (rows.length > 0)
        {
            const row = rows[0] 

            const empregado = row as Empregado

            empregado.planoSaude = await this.planoSaudeProvider.getById(row.planosaudeid)
            
            empregado.departamento = await this.departamentoProvider.getBydId(row.departamentoid)

            return empregado
        }
        else
            return null
    }

    async getByNome(nome:string, sobreNome:string): Promise<Empregado | null> {

        const sql = `
                select
                    id,
                    matricula,
                    nome,
                    sobrenome,
                    departamentoid,
                    telefone,
                    dataadmissao,
                    cargo,
                    escolaridade,
                    sexo,
                    datanascimento,
                    salario,
                    bonus,
                    comissao,
                    planosaudeid
                from
                    empregado
                where
                    nome = ?
                and
                    sobrenome = ?
                `

        const valores: any[] = [nome, sobreNome]

        const conexao = this.dataBase.getConexao()

        const retorno = await conexao.query(sql, valores)

        conexao.end()

        var rows = retorno[0] as RowDataPacket[]

        if (rows.length > 0)
        {
            const row = rows[0]

            const empregado = row as Empregado

            empregado.planoSaude = await this.planoSaudeProvider.getById(row.planosaudeid)

            empregado.departamento = await this.departamentoProvider.getBydId(row.departamentoid)

            return empregado
        }
        else
            return null
    }


    async getAll(): Promise<Empregado[]> {
        const sql = `
                        select
                            id,
                            matricula,
                            nome,
                            sobrenome,
                            departamentoid,
                            telefone,
                            dataadmissao,
                            cargo,
                            escolaridade,
                            sexo,
                            datanascimento,
                            salario,
                            bonus,
                            comissao,
                            planosaudeid
                        from
                            empregado

                        order by
                            nome
                        `

        const valores: any[] = []

        const conexao = this.dataBase.getConexao()

        const retorno = await conexao.query(sql, valores)

        conexao.end()

        var rows = retorno[0] as RowDataPacket[]

        return rows as Empregado[]
    }

    async update(empregado: Empregado): Promise<void> {
        const sql = `
        update empregado
        set
            matricula = ?,
            nome = ?,
            sobrenome = ?,
            departamentoid = ?,
            telefone = ?,
            dataadmissao = ?,
            cargo = ?,
            escolaridade = ?,
            sexo = ?,
            datanascimento = ?,
            salario = ?,
            bonus = ?,
            comissao = ?,
            planosaudeid = ?
        where
            id = ?`

        let valuePlanoSaude = null

        if(empregado.planoSaude != null)
        {
            valuePlanoSaude = empregado.planoSaude.id
        }

        const valores: any[] =
            [
                empregado.matricula,
                empregado.nome,
                empregado.sobrenome,
                (empregado.departamento != null 
                    ? empregado.departamento.id
                    : null),
                empregado.telefone,
                empregado.dataadmissao,
                empregado.cargo,
                empregado.escolaridade,
                empregado.sexo,
                empregado.datanascimento,
                empregado.salario,
                empregado.bonus,
                empregado.comissao,
                valuePlanoSaude,
                empregado.id
            ]

        const conexao = this.dataBase.getConexao()

        await conexao.execute(sql, valores)

        conexao.end()
    }
}