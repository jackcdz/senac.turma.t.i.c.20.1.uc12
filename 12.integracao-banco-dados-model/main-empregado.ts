import { Empregado } from "./models/empregado";
import { DepartamentoProvider } from "./providers/departamento-provider";
import { EmpregadoProvider } from "./providers/empregado-provider";
import { PlanoSaudeProvider } from "./providers/plano-saude-provider";

async function inserirEmpregado() {
    const empregadoProvider = new EmpregadoProvider()
    const planoSaudeProvider = new PlanoSaudeProvider()
    const departamentoProvider = new DepartamentoProvider()

    const planoSaude = await planoSaudeProvider.getById(1)


    const empregado = new Empregado()

    empregado.bonus = 50
    empregado.cargo = "VENDEDOR"
    empregado.comissao = 8
    empregado.dataadmissao = new Date('2020-02-01')
    empregado.datanascimento = new Date('1999-05-01')
    empregado.departamento = await departamentoProvider.getBydId(2)
    empregado.escolaridade = "MEDIO"
    empregado.matricula = "9999999"
    empregado.nome = "BETO"
    empregado.planoSaude = planoSaude
    empregado.salario = 3750
    empregado.sexo = "M"
    empregado.sobrenome = "SILVA"
    empregado.telefone = "54911112233"

    await empregadoProvider.insert(empregado)
}



async function inserirEmpregadoSemPlanoSaude() {
    const empregadoProvider = new EmpregadoProvider()
    const departamentoProvider = new DepartamentoProvider()

    const empregado = new Empregado()

    empregado.bonus = 50
    empregado.cargo = "VENDEDOR"
    empregado.comissao = 8
    empregado.dataadmissao = new Date('2020-02-01')
    empregado.datanascimento = new Date('1999-05-01')
    empregado.departamento = await departamentoProvider.getBydId(2)
    empregado.escolaridade = "MEDIO"
    empregado.matricula = "9999999"
    empregado.nome = "WILLIAN"
    empregado.planoSaude = null
    empregado.salario = 3750
    empregado.sexo = "M"
    empregado.sobrenome = "FERREIRA"
    empregado.telefone = "54911112233"

    await empregadoProvider.insert(empregado)
}

async function getEmpregadoByNome()
{
    const empregadoProvider = new EmpregadoProvider()

    const empregado:Empregado | null = await empregadoProvider.getByNome("Taillom", "Parise")

    console.log(empregado)
}

async function getEmpregadoById()
{
    const empregadoProvider = new EmpregadoProvider()

    const empregado:Empregado | null = await empregadoProvider.getById(3)

    console.log(empregado)

    console.log(empregado?.planoSaude?.descricao)
}

async function alterarPlanoSaudeEmpregado()
{
    const empregadoProvider = new EmpregadoProvider()
    const planoSaudeProvider = new PlanoSaudeProvider()

    const empregado = await empregadoProvider.getByNome("BETO", "SILVA")

    const planoSaude = await planoSaudeProvider.getByDescricao("ipe")

    if(empregado != null)
    {
        empregado.planoSaude = planoSaude

        await empregadoProvider.update(empregado)
    }
}

async function removerPlanoSaudeEmpregado()
{
    const empregadoProvider = new EmpregadoProvider()

    const empregado = await empregadoProvider.getByNome("BETO", "SILVA")

    if(empregado != null)
    {
        empregado.planoSaude = null

        await empregadoProvider.update(empregado)
    }
}

//inserirEmpregadoSemPlanoSaude()

//removerPlanoSaudeEmpregado()

//alterarPlanoSaudeEmpregado()

//getEmpregadoById()

getEmpregadoByNome()

//inserirEmpregado()
