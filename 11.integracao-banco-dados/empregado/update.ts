import { getConexao } from './database'


async function updateEmpregado(
    matricula:string,
    nome:string,
    sobrenome:string,
    departamentoid:number,
    telefone:string,
    dataadmissao:Date,
    cargo:string,
    escolaridade:string,
    sexo:string,
    datanascimento:Date,
    salario:number,
    bonus:number,
    comissao:number,
    planosaudeid:number,
    id:number
)
{
    const conexao = getConexao()

    const sql = `
                update empregado
                set
                    matricula = ?,
                    nome = ?,
                    sobrenome = ?,
                    departamentoid = ?,
                    telefone = ?,
                    dataadmissao = ?,
                    cargo = ?,
                    escolaridade = ?,
                    sexo = ?,
                    datanascimento = ?,
                    salario = ?,
                    bonus = ?,
                    comissao = ?,
                    planosaudeid = ?
                where
                    id = ?
                `

    const valores:any[] = [ 
        matricula, 
        nome, 
        sobrenome, 
        departamentoid, 
        telefone, 
        dataadmissao, 
        cargo, 
        escolaridade, 
        sexo, 
        datanascimento, 
        salario, 
        bonus, 
        comissao, 
        planosaudeid,
        id
    ]

    await conexao.execute(sql, valores)

    conexao.end()  
}

updateEmpregado(
    '1234456'
    ,'Jackson'
    ,'Pimentel'
    ,1
    ,'54996076961'
    ,new Date(2019, 9, 25)
    ,'Docente'
    ,'Superior Completo'
    ,'M'
    ,new Date(1983, 5, 4)
    ,1500
    ,565.58
    ,100
    ,1
    ,7
)