import { getConexao } from './database'
import { ResultSetHeader } from 'mysql2/typings/mysql'


async function inserirEmpregado(
    matricula:string,
    nome:string,
    sobrenome:string,
    departamentoid:number,
    telefone:string,
    dataadmissao:Date,
    cargo:string,
    escolaridade:string,
    sexo:string,
    datanascimento:Date,
    salario:number,
    bonus:number,
    comissao:number,
    planosaudeid:number
)
{
    const conexao = getConexao()

    const sql = `
                insert into empregado
                (
                    matricula,
                    nome,
                    sobrenome,
                    departamentoid,
                    telefone,
                    dataadmissao,
                    cargo,
                    escolaridade,
                    sexo,
                    datanascimento,
                    salario,
                    bonus,
                    comissao,
                    planosaudeid
                )
                values
                (
                    ?,?,?,?,?,?,?,?,?,?,?,?,?,?
                )`

    const valores:any[] = [ matricula, nome, sobrenome, departamentoid, telefone, dataadmissao, cargo, escolaridade, sexo, datanascimento, salario, bonus, comissao, planosaudeid ]

    const retorno = await conexao.execute(sql, valores)

    const result = retorno[0] as ResultSetHeader
    
    console.log(`O id inserido foi ${result.insertId}`)

    conexao.end()  
}

inserirEmpregado(
    '1234456'
    ,'Jackson'
    ,'Pimentel'
    ,1
    ,'54996076961'
    ,new Date(2019, 9, 25)
    ,'Docente'
    ,'Superior Completo'
    ,'M'
    ,new Date(1983, 5, 4)
    ,1000
    ,565.58
    ,0
    ,1
)