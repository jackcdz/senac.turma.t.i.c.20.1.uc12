import { getConexao } from './database'


async function selectByNome(nome:string):Promise<any[]>
{
    const conexao = getConexao()

    const sql = `
                select
                    id,
                    matricula,
                    nome,
                    sobrenome,
                    departamentoid,
                    telefone,
                    dataadmissao,
                    cargo,
                    escolaridade,
                    sexo,
                    datanascimento,
                    salario,
                    bonus,
                    comissao,
                    planosaudeid
                from
                    empregado
                where
                    nome like ?
                `

    const valores:any[] = [ nome ]

    const retorno = await conexao.query(sql, valores)

    conexao.end()  

    console.log(retorno)

    return retorno[0] as any[]

}

selectByNome('%e%')
.then(empregados =>  {
        for (const empregado of empregados) {
            console.log(empregado.nome)
        }
    })

