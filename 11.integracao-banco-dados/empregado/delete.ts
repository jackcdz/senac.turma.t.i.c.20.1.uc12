import { getConexao } from './database'

async function deleteEmpregado(
    id:number
)
{
    const conexao = getConexao()

    const sql = `
                delete from empregado
                where
                    id = ?
                `

    const valores:any[] = [ 
        id
    ]

    await conexao.execute(sql, valores)

    conexao.end()  
}

deleteEmpregado(
    6
)