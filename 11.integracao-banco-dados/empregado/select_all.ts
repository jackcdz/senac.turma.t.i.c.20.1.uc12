import { getConexao } from './database'


async function selectAll():Promise<any[]>
{
    const conexao = getConexao()

    const sql = `
                select
                    id,
                    matricula,
                    nome,
                    sobrenome,
                    departamentoid,
                    telefone,
                    dataadmissao,
                    cargo,
                    escolaridade,
                    sexo,
                    datanascimento,
                    salario,
                    bonus,
                    comissao,
                    planosaudeid
                from
                    empregado
                `

    const valores:any[] = [ ]

    const retorno = await conexao.query(sql, valores)

    conexao.end()  

    return retorno[0] as any[]

}

selectAll()
.then(empregados =>  {
        for (const empregado of empregados) {
            console.log(empregado.nome)
        }
    })

