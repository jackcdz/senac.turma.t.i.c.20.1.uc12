import { createPool, Pool } from 'mysql2/promise'
import { PoolOptions } from 'mysql2/typings/mysql'

export function getConexao():Pool
{
    const options: PoolOptions = {
        host: '127.0.0.1',
        database: 'exercicio_01',
        user: 'root',
        password: '12345678'
    }

    return createPool(options)
}