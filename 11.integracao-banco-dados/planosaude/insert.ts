import { createPool, Pool } from 'mysql2/promise'
import { PoolOptions, ResultSetHeader } from 'mysql2/typings/mysql'

async function inserirPlano():Promise<void> {

    const options: PoolOptions = {
        host: '127.0.0.1',
        database: 'exercicio_01',
        user: 'root',
        password: '12345678'
    }

    const conexao: Pool = createPool(options)

    const sql = ` insert into planosaude
                    (
                        descricao
                    )
                    values
                    (
                        ?
                    )`

    const nomePlanoSaude = 'POW'

    const valores: any[] = [nomePlanoSaude]

    const retorno = await conexao.execute(sql, valores)

    const result = retorno[0] as ResultSetHeader

    console.log(`O id inserido foi ${result.insertId}`)

    conexao.end()   
}

inserirPlano()
