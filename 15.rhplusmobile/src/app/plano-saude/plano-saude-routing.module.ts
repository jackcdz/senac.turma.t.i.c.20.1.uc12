import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlanoSaudePage } from './plano-saude.page';

const routes: Routes = [
  {
    path: '',
    component: PlanoSaudePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlanoSaudePageRoutingModule {}
