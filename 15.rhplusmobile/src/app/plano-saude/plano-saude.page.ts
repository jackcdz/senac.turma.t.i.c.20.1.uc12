import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { PlanoSaudeService } from '../api/plano-saude.service';
import { PlanoSaude } from '../models/plano-saude';

@Component({
  selector: 'app-plano-saude',
  templateUrl: './plano-saude.page.html',
  styleUrls: ['./plano-saude.page.scss'],
})
export class PlanoSaudePage implements OnInit {

  planosSaude:PlanoSaude[] = []

  constructor(
    private navController:NavController,
    private planoSaudeService:PlanoSaudeService,
    private alertController:AlertController,
    private toastController:ToastController) { }

  ngOnInit() {
  }
  

  navPlanoSaudeInserir():void
  {
    this.navController.navigateForward('plano-saude-edicao/')
  }

  navPlanoSaudeAlterar(planoSaude:PlanoSaude):void
  {
    this.navController.navigateForward(`plano-saude-edicao/${planoSaude.id}`)
  }

  async excluirPlanoSaude(planoSaude:PlanoSaude)
  {
    const alert = await this.alertController.create(
      {
        header:"RH Plus",
        message:`Confirma exclusão do plano de saude ${planoSaude.descricao}?`,
        buttons:[
          {
            text:"Cancelar",
            cssClass:"secondary"
          },
          {
            text:"Ok",
            handler:() => this.excluirPlanoSaudeAcao(planoSaude)
          }
        ]
      }
    )

    await alert.present()    
  }

  async excluirPlanoSaudeAcao(planoSaude: PlanoSaude)
  {
    const resposta = await this.planoSaudeService
                      .delete(planoSaude)

    if(resposta.success)
      this.ionViewDidEnter()
    else
      this.showToast(resposta.message)
  }

  async showToast(message:string)
  {
    const toast = await this.toastController.create(
      {
        message,
        duration:60000,
        position:'middle'
      }
    )

    toast.present();
  }


  ionViewDidEnter()
  {
    this.planoSaudeService.getAll()
    .then(resposta => this.planosSaude = resposta.data)
  }

}
