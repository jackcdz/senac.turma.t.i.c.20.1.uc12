import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PlanoSaudePageRoutingModule } from './plano-saude-routing.module';

import { PlanoSaudePage } from './plano-saude.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PlanoSaudePageRoutingModule
  ],
  declarations: [PlanoSaudePage]
})
export class PlanoSaudePageModule {}
