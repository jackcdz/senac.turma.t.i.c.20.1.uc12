import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { GlobalVariables } from '../GlobalVariabels';
import { Usuario } from '../models/usuario';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  usuario:Usuario

  constructor(private navController:NavController) {}

  ngOnInit() {
    this.usuario = GlobalVariables.usuarioLogado
  }

  onClickPlanoSaude():void
  {
    this.navController.navigateForward('plano-saude')
  }

  exit()
  {
    GlobalVariables.usuarioLogado = null
    this.navController.navigateForward('logon')
  }
}
