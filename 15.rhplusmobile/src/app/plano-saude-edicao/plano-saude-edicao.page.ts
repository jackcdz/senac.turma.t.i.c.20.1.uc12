import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { PlanoSaudeService } from '../api/plano-saude.service';
import { PlanoSaude } from '../models/plano-saude';
import { Resposta } from '../models/resposta';

@Component({
  selector: 'app-plano-saude-edicao',
  templateUrl: './plano-saude-edicao.page.html',
  styleUrls: ['./plano-saude-edicao.page.scss'],
})
export class PlanoSaudeEdicaoPage implements OnInit {

  planoSaude:PlanoSaude

  constructor(
    private activateRoute: ActivatedRoute,
    private planoSaudeService:PlanoSaudeService,
    private navController:NavController,
    private toastController:ToastController
    ) 
  { 
    let id:string = this.activateRoute.snapshot.paramMap.get('id')

    this.planoSaude = {
      descricao:"",
      id:0
    }

    if(id != '')
    {
      const idNumero:number = parseInt(id)

      this.planoSaudeService.getById(idNumero)
      .then(resposta => this.planoSaude = resposta.data)
    }
  }

  ngOnInit() {
  }

  async showToast(message:string)
  {
    const toast = await this.toastController.create(
      {
        message,
        duration:3000,
        position:'middle'
      }
    )

    toast.present();
  }

  async salvar()
  {
    let resposta:Resposta<PlanoSaude>

    if(this.planoSaude.id == 0)
      resposta = await this.planoSaudeService
        .create(this.planoSaude)
    else
      resposta = await this.planoSaudeService
        .update(this.planoSaude)

    if(resposta.success)  
        this.navController.navigateBack('plano-saude')
    else
      this.showToast(resposta.message)
  }

}
