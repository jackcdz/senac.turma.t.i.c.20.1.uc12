import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PlanoSaudeEdicaoPageRoutingModule } from './plano-saude-edicao-routing.module';

import { PlanoSaudeEdicaoPage } from './plano-saude-edicao.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PlanoSaudeEdicaoPageRoutingModule
  ],
  declarations: [PlanoSaudeEdicaoPage]
})
export class PlanoSaudeEdicaoPageModule {}
