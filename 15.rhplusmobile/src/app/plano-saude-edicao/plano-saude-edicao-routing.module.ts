import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlanoSaudeEdicaoPage } from './plano-saude-edicao.page';

const routes: Routes = [
  {
    path: '',
    component: PlanoSaudeEdicaoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlanoSaudeEdicaoPageRoutingModule {}
