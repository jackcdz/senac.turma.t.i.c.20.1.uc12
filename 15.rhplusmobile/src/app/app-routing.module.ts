import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { CheckLoginGuard } from './check-login.guard';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule),
    canActivate:[CheckLoginGuard]
  },
  {
    path: '',
    redirectTo: 'logon',
    pathMatch: 'full'
  },
  {
    path: 'plano-saude',
    loadChildren: () => import('./plano-saude/plano-saude.module').then( m => m.PlanoSaudePageModule),
    canActivate:[CheckLoginGuard]
  },
  {
    path: 'plano-saude-edicao/:id',
    loadChildren: () => import('./plano-saude-edicao/plano-saude-edicao.module').then( m => m.PlanoSaudeEdicaoPageModule),
    canActivate:[CheckLoginGuard]
  },
  {
    path: 'logon',
    loadChildren: () => import('./logon/logon.module').then( m => m.LogonPageModule)
  },
  {
    path: 'registro-usuario',
    loadChildren: () => import('./registro-usuario/registro-usuario.module').then( m => m.RegistroUsuarioPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
