import { Injectable } from '@angular/core';
import Axios, { AxiosInstance } from 'axios';
import { Resposta } from '../models/resposta';
import { Usuario } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private axiosInstance:AxiosInstance

  constructor() {
    this.axiosInstance = Axios.create({
      baseURL:'http://127.0.0.1:3000/usuario'
    })
  }


  async create(usuario:Usuario):Promise<Resposta<Usuario>>
  {
    const resultAxios = await this.axiosInstance
      .post('/create', usuario)

    return resultAxios.data
  }

  async validarLogin(email:string, senha:string):Promise<Resposta<Usuario>>
  {
      const login = {
        email,
        senha
      }

      const resultAxios = await this.axiosInstance
        .post('/validarlogin', login)

      return resultAxios.data
  }
}
