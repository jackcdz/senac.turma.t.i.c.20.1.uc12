
export class Pessoa
{
    //FUNCAO SINCRONA
    obterNomeSincrono(id:number):String
    {
        if(id == 1)
            return "Jackson"
        else if(id == 2)
            return "Luis"
        else if(id == 3)
            return "Pimentel"

        return "NOT_FOUND"
    }


    //FUNCAO ASSINCRONA
    obterNomeAssincrono(id:number, 
            successCallBack:(nome:string) => void,
            failureCallBack:(descricao:string) => void):void
    {
        setTimeout(() =>
        {
            let nome = "";

            if(id == 1)
                nome = "Jackson"
            else if(id == 2)
                nome = "Luis"
            else if(id == 3)
                nome = "Pimentel"
            
            if(nome != "")
                successCallBack(nome)
            else
                failureCallBack('NOT_FOUND')
            
        }, 5000)
    }    

    //FUNCAO ASSINCRONA COM PROMISE
    async obterNomeAssincronoComPromise(id:number):Promise<string>
    {
        return new Promise<string>((success, failure) =>
        {
            setTimeout(() =>
            {
                let nome = "";
    
                if(id == 1)
                    nome = "Jackson"
                else if(id == 2)
                    nome = "Luis"
                else if(id == 3)
                    nome = "Pimentel"
                
                if(nome != "")
                    success(nome)
                else
                    failure(new Error('NOT_FOUND'))
                
            }, 5000)

        })
    }

}