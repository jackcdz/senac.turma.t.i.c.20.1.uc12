import { Pessoa } from './pessoa'

let p = new Pessoa()

console.log('antes')

//CALLBACK
let successNomeCallBack = (nome:String):void =>
{
    console.log(nome)
}

let failureNomeCallBack = (description:string):void =>
{
    console.log('OPS.. OCORREU UM ERRO. ' + description)
}

//let nome = p.obterNomeSincrono(2)
//console.log(nome)

//Chama funcao Assincrona
//p.obterNomeAssincrono(4, successNomeCallBack, failureNomeCallBack)

//let promessa = p.obterNomeAssincronoComPromise(4)

//promessa.then((nome:string) => { console.log(nome) })
//        .catch((e:Error) => { console.log(e)})

//PROMESSA SIMPLIFICADA
p.obterNomeAssincronoComPromise(2)
    .then((nome:string) => { console.log(nome) })
    .catch((e:Error) => { console.log(e)})

console.log('depois')

/*
FORÇANDO EXECUTAR UM METODO ASSINCRONO DE FORMA SINCRONA (await)
async function executaAsinc()
{
    await p.obterNomeAssincronoComPromise(2)
    .then((nome:string) => { console.log(nome) })
    .catch((e:Error) => { console.log(e)})

    console.log('depois')
}

executaAsinc();*/




