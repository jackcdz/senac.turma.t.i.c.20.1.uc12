import { Injectable } from '@angular/core';
import { PlanoSaude } from '../models/PlanoSaude';
import Axios, { AxiosInstance } from 'axios';
import { Resposta } from '../models/Resposta';

@Injectable({
  providedIn: 'root'
})
export class PlanoSaudeService {

  private axiosInstance:AxiosInstance

  constructor() {
    this.axiosInstance = Axios.create({
      baseURL:'http://127.0.0.1:3000/plano-saude'
    })
  }
  async getAll():Promise<Resposta<PlanoSaude[]>>
  {
    const resultAxios = await this.axiosInstance.get('/getall')

    return resultAxios.data
  }
}
