import { Component, OnInit } from '@angular/core';
import { PlanoSaude } from 'src/app/models/PlanoSaude';
import { PlanoSaudeService } from 'src/app/services/plano-saude.service';

@Component({
  selector: 'app-lista-planos',
  templateUrl: './lista-planos.component.html',
  styleUrls: ['./lista-planos.component.css']
})
export class ListaPlanosComponent implements OnInit {

  planosSaude:PlanoSaude[]

  constructor(private planoSaudeService:PlanoSaudeService) 
  {
    
  }

  ngOnInit(): void {
    this.planoSaudeService.getAll()
    .then(r => this.planosSaude = r.data)
  }

}
