import { Component, OnInit } from '@angular/core';
import { Pessoa } from 'src/app/models/Pessoa';

@Component({
  selector: 'app-lista-pessoas',
  templateUrl: './lista-pessoas.component.html',
  styleUrls: ['./lista-pessoas.component.css']
})
export class ListaPessoasComponent implements OnInit {

  pessoas:Pessoa[] = []
  
  constructor() { 
  }

  ngOnInit(): void {
    this.pessoas.push({
      codigo:"1",
      cpf:"003.672.400-92",
      dataNascimento:"04/06/1983",
      nome:"jackson"
    })

    this.pessoas.push({
      codigo:"2",
      cpf:"000.111.222-33",
      dataNascimento:"01/06/1984",
      nome:"yan"
    })

  }

  addPessoaClick()
  {
    this.pessoas.push({
      codigo:"3",
      cpf:"775.663.447-58",
      dataNascimento:"01/05/1990",
      nome:"Tailon"
    })

  }

}
