import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { PlanoSaudeEdicaoComponent } from './pages/plano-saude/plano-saude-edicao/plano-saude-edicao.component';
import { PlanoSaudeListComponent } from './pages/plano-saude/plano-saude-list/plano-saude-list.component';
import { PlanoSaudeComponent } from './pages/plano-saude/plano-saude.component';

const routes: Routes = [
  {
    path:'', redirectTo:'/home', pathMatch:'full'
  },
  {
    path:'home', component:HomePageComponent
  },
  {
    path:'plano-saude', component:PlanoSaudeComponent,
    children:
    [
      {
        path:'', redirectTo:'/plano-saude/plano-saude-list', pathMatch:'full'
      },
      {
        path:'plano-saude-list', component:PlanoSaudeListComponent
      },
      {
        path:'plano-saude-edicao/:id', component:PlanoSaudeEdicaoComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
