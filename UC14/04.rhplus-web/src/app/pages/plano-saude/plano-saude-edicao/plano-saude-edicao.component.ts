import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PlanoSaude } from 'src/app/models/plano-saude';
import { PlanoSaudeService } from 'src/app/services/plano-saude.service';

@Component({
  selector: 'app-plano-saude-edicao',
  templateUrl: './plano-saude-edicao.component.html',
  styleUrls: ['./plano-saude-edicao.component.css']
})
export class PlanoSaudeEdicaoComponent implements OnInit {

  planoSaude:PlanoSaude

  constructor(
    private planoSaudeService:PlanoSaudeService,
    private activatedRoute:ActivatedRoute,
    private router:Router) { 

      this.planoSaude = {
        descricao:'',
        id:0,
        valor:0
      }

      const idParam = this.activatedRoute.snapshot.params['id']

      if(idParam != '0')
      {
        this.planoSaudeService.getById(parseInt(idParam))
        .then(resposta =>
          {
            this.planoSaude = resposta.data
          })
      }
    }

  ngOnInit(): void {
  }

  async salvar()
  {
    if(this.planoSaude.id > 0)
    {
      await this.planoSaudeService.update(this.planoSaude)
    }
    else
    {
      await this.planoSaudeService.create(this.planoSaude)
    }

    this.router.navigate(['/plano-saude/plano-saude-list'])
  }

}
