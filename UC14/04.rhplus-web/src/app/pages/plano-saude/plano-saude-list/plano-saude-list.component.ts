import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PlanoSaude } from 'src/app/models/plano-saude';
import { PlanoSaudeService } from 'src/app/services/plano-saude.service';

@Component({
  selector: 'app-plano-saude-list',
  templateUrl: './plano-saude-list.component.html',
  styleUrls: ['./plano-saude-list.component.css']
})
export class PlanoSaudeListComponent implements OnInit {

  planosSaude:PlanoSaude[] = []

  paginaAtual:number = 1
  
  quantidadePagina:number = 3

  disabledPrevious:string = "disabled"

  disabledNext:string = ""

  numeroPaginas:number = 0

  constructor(private planoSaudeService:PlanoSaudeService, private router:Router) { }

  async ngOnInit(): Promise<void> {

    const resposta = await this.planoSaudeService.getAll()

    if(resposta.success)
      this.planosSaude = resposta.data
  }

  navEditarPlanoSaude(planoSaude:PlanoSaude)
  {
    this.router.navigate([`/plano-saude/plano-saude-edicao/${planoSaude.id}`])
  }

  criarNovo()
  {
    this.router.navigate([`/plano-saude/plano-saude-edicao/0`])
  }

  async deletePlanoSaude(planoSaude:PlanoSaude)
  {
    await this.planoSaudeService.delete(planoSaude)

    this.ngOnInit()
  }

  getRegistrosPagina():PlanoSaude[]
  {
    const planosSaudesPagina:PlanoSaude[] = []

    const final = this.quantidadePagina * this.paginaAtual
    const inicial = final - this.quantidadePagina

    for(let i = inicial; i < final; i++)
    {
      if(i < this.planosSaude.length)
        planosSaudesPagina.push(this.planosSaude[i])
    }

    return planosSaudesPagina
  }

  getPaginas():number[]
  {
    const paginas:number[] = []

    const n = this.planosSaude.length / this.quantidadePagina

    const nTruncado = Math.trunc(n)

    this.numeroPaginas = n == nTruncado ? n : (nTruncado + 1)

    for(let i = 1; i <= this.numeroPaginas; i++)
      paginas.push(i)

    return paginas
  }

  setarPagina(pagina:number)
  {
    this.paginaAtual = pagina
    this.atualizarDisableds()
  }

  proximaPagina()
  {
    this.paginaAtual++
    this.atualizarDisableds()
  }

  paginaAnterior()
  {
    if(this.paginaAtual == 1)
      return;

    this.paginaAtual--

    this.atualizarDisableds()
  }

  atualizarDisableds()
  {
    this.disabledPrevious = this.paginaAtual == 1 ? "disabled" : ""
    this.disabledNext = this.paginaAtual == this.numeroPaginas ? "disabled" : ""
  }

}
