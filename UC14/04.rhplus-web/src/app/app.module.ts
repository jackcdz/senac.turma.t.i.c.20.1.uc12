import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { PlanoSaudeComponent } from './pages/plano-saude/plano-saude.component';
import { HeaderComponent } from './components/header/header.component';
import { PlanoSaudeListComponent } from './pages/plano-saude/plano-saude-list/plano-saude-list.component';
import { PlanoSaudeEdicaoComponent } from './pages/plano-saude/plano-saude-edicao/plano-saude-edicao.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    PlanoSaudeComponent,
    HeaderComponent,
    PlanoSaudeListComponent,
    PlanoSaudeEdicaoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
