import { Injectable } from '@angular/core';
import Axios, { AxiosInstance } from 'axios';
import { PlanoSaude } from '../models/plano-saude';
import { Resposta } from '../models/resposta';

@Injectable({
  providedIn: 'root'
})
export class PlanoSaudeService {

  private axiosInstance:AxiosInstance

  constructor() {
    this.axiosInstance = Axios.create({
      baseURL:'http://127.0.0.1:3000/plano-saude'
    })
  }

  async getAll():Promise<Resposta<PlanoSaude[]>>
  {
    const resultAxios = await this.axiosInstance.get('/getall')

    return resultAxios.data
  }

  async getById(id:number):Promise<Resposta<PlanoSaude>>
  {
    const resultAxios = await this.axiosInstance.get('/getbyid/' + id)

    return resultAxios.data
  }

  async create(planoSaude:PlanoSaude):Promise<Resposta<PlanoSaude>>
  {
    const resultAxios = await this.axiosInstance
      .post('/create', planoSaude)

    return resultAxios.data
  }

  async update(planoSaude:PlanoSaude):Promise<Resposta<PlanoSaude>>
  {
    const resultAxios = await this.axiosInstance
      .put('/update', planoSaude)

    return resultAxios.data
  }

  async delete(planoSaude:PlanoSaude):Promise<Resposta<PlanoSaude>>
  {
    const resultAxios = await this.axiosInstance
      .put(`/delete/${planoSaude.id}`)

    return resultAxios.data

  }

}
