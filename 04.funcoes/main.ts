let nome = "a"

function validarNome(nome:any)
{
    nome = nome.trim()

    if(nome.length < 3)
    {
        throw new Error('Nome deve ter no minimo 3 caracteres')
    }

    console.log("CADASTRADO COM SUCESSO")
}



function validarCPF(cpf:string)
{
    cpf = cpf.trim()

    let somaDigito1 = 0
    let somaDigito2 = 0

    for(let i = 0; i < cpf.length; i++)
    {
        const caracter = cpf.substring(i , i + 1)

        const digito = parseInt(caracter)

        const multiplicacao1 = digito * (11 - (i + 1))

        const multiplicacao2 = digito * (11 - i)

        if(i <= 8)
            somaDigito1 = somaDigito1 + multiplicacao1
        
        if(i <= 9)
            somaDigito2 = somaDigito2 + multiplicacao2
    }

    const resto1 = somaDigito1 % 11
    const digito1Valida = resto1 < 2 ? 0 : (11 - resto1)
    const caracter1Valida = parseInt(cpf.substring(9, 10))

    const resto2 = somaDigito2 % 11
    const digito2Valida = resto2 < 2 ? 0 : (11 - resto2)
    const caracter2Valida = parseInt(cpf.substring(10, 11))

    if(digito1Valida != caracter1Valida
        || digito2Valida != caracter2Valida
        )
        {
            throw new Error("CPF Invalido")
        }


    console.log('CPF Valido')
}

import fs from 'fs'

try {
    //validarNome(nome)    
    validarCPF('00367840092')
} catch (err) {
    console.log(err.message)
    
    fs.appendFile('c:/temp/logmain.txt', err as string, () => {})
    
    console.log('OCORRERAM ERROS AO VALIDAR O NOME')
}

