import { Body, Controller, Get, Param, Post, Put, Query } from '@nestjs/common';
import { PlanoSaude } from 'src/models/plano-saude';
import { Resposta } from 'src/models/resposta';
import { PlanoSaudeProvider } from 'src/providers/plano-saude-provider';
import { PlanoSaudeService } from 'src/services/plano-saude/plano-saude.service';

@Controller('plano-saude')
export class PlanoSaudeController {

    constructor(private planoSaudeProvider: PlanoSaudeProvider
        , private planoSaudeService:PlanoSaudeService) {
    }

    @Get('getbyid/:id')
    async getById(@Param('id') idParam: string): Promise<Resposta> {
        const id = parseInt(idParam)

        try {
            if( id < 0)
                throw new Error("Id negativo não é permitido");
                
            const planoSaude = await this.planoSaudeProvider.getById(id)

            return {
                success: true,
                data: planoSaude,
                message: ''
            }
        } catch (error) {
            return {
                success: false,
                data: null,
                message:error.message
            }
        }        
    }


    @Get('getbydescricao')
    async getByDescricao(@Query('descricao') descricaoParam: string): Promise<PlanoSaude> {
        return this.planoSaudeProvider.getByDescricao(descricaoParam)
    }

    @Get('getall')
    async getAll(): Promise<Resposta> {
        try {
            const planosSaude = await this.planoSaudeProvider.getAll()

            return {
                success: true,
                data: planosSaude,
                message: ''
            }
        } catch (error) {
            return {
                success: false,
                data: null,
                message:error.message
            }
        }
    }

    @Post('create')
    async create(@Body() planoSaude:PlanoSaude):Promise<Resposta>
    {
        try {
            planoSaude.id = 0

            const planoSaudeSalvo = await this.planoSaudeService.save(planoSaude)
            
            return {
                data:planoSaudeSalvo,
                message:'Plano de saude criado com sucesso',
                success:true
            }

        } catch (error) {
            return {
                data:null,
                message:error.message,
                success:false
            }
        }
    }

    @Put('update')
    async update(@Body() planoSaude:PlanoSaude):Promise<Resposta>
    {
        try {

            const planoSaudeExiste = await this.planoSaudeProvider.getById(planoSaude.id)

            if(planoSaudeExiste == null)
                throw new Error(`Plano de Saude com id ${planoSaude.id} não encontrado`);
                

            const planoSaudeSalvo = await this.planoSaudeService.save(planoSaude)

            return {
                data:planoSaudeSalvo,
                message:'Plano de saude alterado com sucesso',
                success:true
            }

        } catch (error) {
            return {
                data:null,
                message:error.message,
                success:false
            }
        }
    }

    @Put('delete/:id')
    async delete(@Param('id') idParam: string):Promise<Resposta>
    { 
        const id = parseInt(idParam)

        try {

            const planoSaudeExiste = await this.planoSaudeProvider.getById(id)

            if(planoSaudeExiste == null)
                throw new Error(`Plano de Saude com id ${id} não encontrado`);

            await this.planoSaudeService.delete(planoSaudeExiste)

            return {
                data:planoSaudeExiste,
                message:'Plano de saude deletado com sucesso',
                success:true
            }

        } catch (error) {
            return {
                data:null,
                message:error.message,
                success:false
            }
        }
    }


}
