import { Body, Controller, Post } from '@nestjs/common';
import { Login } from 'src/models/login';
import { Resposta } from 'src/models/resposta';
import { Usuario } from 'src/models/usuario';
import { UsuarioService } from 'src/services/usuario/usuario.service';

@Controller('usuario')
export class UsuarioController {

    constructor(private usuarioService:UsuarioService)
    {

    }

    @Post('savepessoa')
    savePessoa(@Body() pessoa:any)
    {
        console.log(pessoa)
    }


    @Post('create')
    async create(@Body() usuario:Usuario):Promise<Resposta>
    {
        try {
            usuario.id = 0

            const usuarioSalvo = await this.usuarioService.save(usuario)
            
            return {
                data:usuarioSalvo,
                message:'Usuario criado com sucesso',
                success:true
            }

        } catch (error) {
            return {
                data:null,
                message:error.message,
                success:false
            }
        }
    }


    @Post('validarlogin')
    async validarLogin(@Body() login:Login):Promise<Resposta>
    {
        try {

            const usuario = await this.usuarioService.validarLogin(login)
            
            return {
                data:usuario,
                message:'Login efetuado com sucesso',
                success:true
            }

        } catch (error) {
            return {
                data:null,
                message:error.message,
                success:false
            }
        }
    }

}
