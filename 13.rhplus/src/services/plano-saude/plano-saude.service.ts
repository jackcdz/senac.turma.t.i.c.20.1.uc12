import { Injectable } from '@nestjs/common';
import { PlanoSaude } from 'src/models/plano-saude';
import { PlanoSaudeProvider } from 'src/providers/plano-saude-provider';

@Injectable()
export class PlanoSaudeService {

    constructor(private planoSaudeProvider:PlanoSaudeProvider) {
        
    }

    async save(planoSaude:PlanoSaude):Promise<PlanoSaude>
    {
        //planoSaude.descricao = planoSaude.descricao.trim()

        //if(planoSaude.descricao.length < 3)
            //throw new Error("Descrição deve conter no minimo 3 caracteres");
        
        if(planoSaude.id == 0)
        {
            await this.planoSaudeProvider.insert(planoSaude)
        }
        else
        {
            await this.planoSaudeProvider.update(planoSaude)
        }

        return planoSaude
    }

    async delete(planoSaude:PlanoSaude):Promise<void>
    {
        await this.planoSaudeProvider.delete(planoSaude)
    }


}
