import { Injectable, Scope } from '@nestjs/common';
import { createPool, Pool } from 'mysql2/promise'
import { PoolOptions } from 'mysql2/typings/mysql'

@Injectable({scope:Scope.REQUEST})
export class DataBase {
    getConexao():Pool
    {
        const options: PoolOptions = {
            host: '127.0.0.1',
            database: 'exercicio_01',
            user: 'root',
            password: '12345678',
            typeCast:((field: any, next: () => void) => {
                if (field.type == "NEWDECIMAL") {
                    var value = field.string();
                    return (value === null) ? null : Number(value);
                }

                return next()
            })
        }

        return createPool(options)
    }
}
