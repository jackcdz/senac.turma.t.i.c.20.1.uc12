import { Injectable } from '@nestjs/common';
import { DataBase } from './data-base';
import { ResultSetHeader, RowDataPacket } from 'mysql2/typings/mysql'
import { Usuario } from 'src/models/usuario';

@Injectable()
export class UsuarioProvider {

    constructor(private dataBase: DataBase) {
    }

    async insert(usuario: Usuario): Promise<void> {
        const sql = `
                    insert into usuario
                    (
                        nome,
                        email,
                        senha
                    )
                    values
                    (
                        ?,?,?
                    )`

        const valores: any[] =
            [
                usuario.nome,
                usuario.email,
                usuario.senha
            ]

        const conexao = this.dataBase.getConexao()

        const retorno = await conexao.execute(sql, valores)

        const result = retorno[0] as ResultSetHeader

        usuario.id = result.insertId

        conexao.end()
    }

    async getById(id: number): Promise<Usuario | null> {

        const sql = `
                select
                    id,
                    nome,
                    email,
                    senha
                from
                    usuario
                where
                    id = ?
                `

        const valores: any[] = [id]

        const conexao = this.dataBase.getConexao()

        const retorno = await conexao.query(sql, valores)

        conexao.end()

        var rows = retorno[0] as RowDataPacket[]

        if (rows.length > 0)
            return rows[0] as Usuario
        else
            return null
    }

    async getByEmail(email:string): Promise<Usuario | null> {

        const sql = `
                    select
                        id,
                        nome,
                        email,
                        senha
                    from
                        usuario
                    where
                        email = ?
                                `

        const valores: any[] = [email]

        const conexao = this.dataBase.getConexao()

        const retorno = await conexao.query(sql, valores)

        conexao.end()

        var rows = retorno[0] as RowDataPacket[]

        if (rows.length > 0)
            return rows[0] as Usuario
        else
            return null
    }

    async getAll(): Promise<Usuario[]> {
        const sql = `
                        select
                            id,
                            nome,
                            email,
                            senha
                        from
                            usuario

                        order by
                            nome
                        `

        const valores: any[] = []

        const conexao = this.dataBase.getConexao()

        const retorno = await conexao.query(sql, valores)

        conexao.end()

        var rows = retorno[0] as RowDataPacket[]

        return rows as Usuario[]
    }
}
