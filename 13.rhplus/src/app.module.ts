import { Module } from '@nestjs/common';
import { PlanoSaudeProvider } from './providers/plano-saude-provider';
import { PlanoSaudeService } from './services/plano-saude/plano-saude.service';
import { PlanoSaudeController } from './controllers/plano-saude/plano-saude.controller';
import { DataBase } from './providers/data-base';
import { UsuarioProvider } from './providers/usuario-provider';
import { UsuarioService } from './services/usuario/usuario.service';
import { UsuarioController } from './controllers/usuario/usuario.controller';

@Module({
  imports: [],
  controllers: [PlanoSaudeController, UsuarioController],
  providers: [ PlanoSaudeProvider, PlanoSaudeService, DataBase, UsuarioProvider, UsuarioService],
})
export class AppModule {}
