Repositório da Turma Senac.Turma.T.I.C.20.1.UC12


NestJS
https://nestjs.com/

Criar uma nova api
	nest new #NOME_API
	
Criar um nova controller
	nest g controller #NOME_CONTROLLER
	
Criar um nova service
	nest g service #NOME_SERVICE
	
Criar um nova provider
	nest g provider #NOME_PROVIDER
	

Exemplo dados JSON
{

	"descricao":"paster",
	"preco":6,
	"dataCadastro":"2020-08-25 21:55:00GMT",
	"ativo":true	
}