import { Module } from '@nestjs/common';
import { ContaBancariaProvider } from './providers/conta-bancaria-provider';
import { ContaBancariaService } from './services/conta-bancaria/conta-bancaria.service';
import { ContaBancariaController } from './controllers/conta-bancaria/conta-bancaria.controller';
import { PessoaController } from './pessoa/pessoa.controller';
import { PessoaController } from './controllers/pessoa/pessoa.controller';

@Module({
  imports: [],
  controllers: [ContaBancariaController, PessoaController],
  providers: [ContaBancariaProvider, ContaBancariaService],
})
export class AppModule {}
