import { Injectable } from '@nestjs/common';
import { ContaBancaria } from 'src/models/conta-bancaria';

@Injectable()
export class ContaBancariaProvider {

    private static contas:ContaBancaria[] = []
    private static sequence:number = 0
    

    async insert(contaBancaria:ContaBancaria):Promise<ContaBancaria>
    {
        return new Promise((success, failed) =>
        {
            contaBancaria.id = ++ContaBancariaProvider.sequence

            ContaBancariaProvider.contas.push(contaBancaria)

            success(contaBancaria)
        // INSERT INTO CONTABANCARIA
        // CONEXAO COM BANCO

        })
    }

    async update(contaBancaria:ContaBancaria):Promise<ContaBancaria>
    {
        return new Promise((success, failed) =>
        {
            for (let index = 0; index < ContaBancariaProvider.contas.length; index++) {
                const element = ContaBancariaProvider.contas[index];

                if(element.id == contaBancaria.id)
                {
                    ContaBancariaProvider.contas[index] = contaBancaria
                }
            }

            success(contaBancaria)

            // UPDATE CONTABANCARIA
            // CONEXAO COM BANCO
        })
    }

    async getById(id:number):Promise<ContaBancaria>
    {
        return new Promise((success, failed) =>
        {
            for (const conta of ContaBancariaProvider.contas) {
                if(conta.id == id)
                {
                    success(conta)
                    return
                }
            }

            success(null)
            
            // SELECT * FROM CONTA_BANCARIA WHERE ID = ?
            // CONEXAO COM BANCO
        })
    }
    
    async getAll():Promise<ContaBancaria[]>
    {
        return new Promise((success, failed) =>
        {
            success(ContaBancariaProvider.contas)
            // SELECT * FROM CONTA_BANCARIA
            // CONEXAO COM BANCO
        })
    }
    

}




