import { Injectable } from '@nestjs/common';
import { ContaBancariaProvider } from 'src/providers/conta-bancaria-provider';
import { ContaBancaria } from 'src/models/conta-bancaria';
import { exception } from 'console';

@Injectable()
export class ContaBancariaService {

    constructor(private contaBancariaProvider:ContaBancariaProvider)
    {

    }

    async save(contaBancaria:ContaBancaria):Promise<ContaBancaria>
    {

        if(contaBancaria.id == 0)
            return this.contaBancariaProvider.insert(contaBancaria)
        else
            return this.contaBancariaProvider.update(contaBancaria)
    }
}
