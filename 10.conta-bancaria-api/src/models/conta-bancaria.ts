
export class ContaBancaria
{
    id:number
    numero:number
    digito:string   
    agencia:number
    agenciaDigito:string
    titular:string
    saldo:number
    banco:string
    dataAbertura:Date
}