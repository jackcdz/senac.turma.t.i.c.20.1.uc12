import { Controller, Post, Body, Put, Param, Get, Query } from '@nestjs/common';
import { ContaBancariaService } from 'src/services/conta-bancaria/conta-bancaria.service';
import { ContaBancariaProvider } from 'src/providers/conta-bancaria-provider';
import { ContaBancaria } from 'src/models/conta-bancaria';

@Controller('conta-bancaria')
export class ContaBancariaController 
{
    constructor(
        private contaBancariaService:ContaBancariaService,
        private contaBancariaProvider:ContaBancariaProvider        
        )
    {
    }

    //http://localhost:3000/conta-bancaria/create
    @Post('create')
    async create(@Body() contaBancaria:ContaBancaria):Promise<ContaBancaria>
    {   
        contaBancaria.id = 0
        return this.contaBancariaService.save(contaBancaria)
    }

    //http://localhost:3000/conta-bancaria/update
    @Put('update/:id')
    async update(@Body() contaBancaria:ContaBancaria, @Param('id') idParam:string):Promise<ContaBancaria>
    {
        contaBancaria.id = parseInt(idParam)

        return this.contaBancariaService.save(contaBancaria)
    }

    //http://localhost:3000/conta-bancaria/getall
    @Get('getall')
    async getAll():Promise<ContaBancaria[]>
    {
        return this.contaBancariaProvider.getAll()        
    }

    //http://localhost:3000/conta-bancaria/getbyid?id=8
    @Get('getbyid')
    async getById(@Query('id') id:number):Promise<ContaBancaria>
    {
        return this.contaBancariaProvider.getById(id)
    }
}
